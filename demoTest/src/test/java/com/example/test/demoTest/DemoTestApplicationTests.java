package com.example.test.demoTest;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoTestApplicationTests {

	@Autowired
	Resource resource;

	@Test()
	public void notNullParameter(){
		int result = resource.binaryToDecimal(null);
		Assert.assertEquals(result,0);
	}
	@Test()
	public void validNumberZero(){
		int result = resource.binaryToDecimal("0");
		Assert.assertEquals(result,0);
	}

	@Test
	public void validNumberOne(){
		int result = resource.binaryToDecimal("1");
		Assert.assertEquals(result,1);
	}
	@Test
	public void validNumberTwo(){
		int result = resource.binaryToDecimal("10");
		Assert.assertEquals(result,2);
	}
	@Test
	public void validNumberThree(){
		int result = resource.binaryToDecimal("11");
		Assert.assertEquals(result,3);
	}
	@Test
	public void validNumberFour(){
		int result = resource.binaryToDecimal("100");
		Assert.assertEquals(result,4);
	}
	@Test
	public void validNumberLarge(){
		int result = resource.binaryToDecimal("1101001001100101010101");
		Assert.assertEquals(result,3447125);
	}


}

